#a command-line utility for retrieving Nextbus arrival predictions
from BeautifulSoup import BeautifulSoup, SoupStrainer
from urllib import urlopen
import time
import re

class Route:
	"""Container for a single direction of a particular AC Transit route
	"""
	
	def __init__(self, route_name):
		self.route_name = route_name
		base_route = "http://www.nextbus.com/predictor/simpleStopSelector.shtml?a=actransit&r="
		self.soup = BeautifulSoup(urlopen(base_route + route_name))
		self.directionslist = []
		routedirs = self.soup.findAll('td', {'class':'simstopSelectorTH'})
		for d in routedirs:
			strong = d.find('strong')
			if strong != None:
				self.directionslist.append(strong.string)
	
	def get_estimate(self, route_url):
		"""
		Returns a list of predicted minute arrival times for the route. Length of the string is unspecified.
		"""
		base_route = "http://www.nextbus.com/predictor/"
		#print base_route + route_url
		arrival_page = BeautifulSoup(urlopen(base_route+route_url))
		predictions = arrival_page.findAll('td', {'class':re.compile("predictionNumber.?")})
		result = []
		for n in predictions:
			result.append(n.find('div').string.split(';')[1].strip())
		return result

	def populate_stops(self, direction):
		"""Once populate_stops is called with a direction the Route is associated with 
			that direction only
		"""

		self.direction = direction
		stops = {}
		routedirs = self.soup.findAll('td', {'class':'simstopSelectorTH'})
		column = 0
		for r in routedirs:
			direction_name = r.find('strong')
			if direction_name != None:
				direction_name = direction_name.string
				if direction_name == direction:
					break
			column += 1
		#column indicates whether the route's stops are in the first or second column on the page
		stopcolumns = self.soup.findAll('td', {'class':'simstopSelectorTD top'})
		stoparray = []
		col = stopcolumns[0]
		if column == 2:
			col = stopcolumns[1]

		for stop in col.findAll('td'):
			stoplink = None
			for link in stop.findAll('a'):
				stoplink = link['href']
			stopname = ""
			for name in stop.findAll('nobr'):
				if name == None:
					continue
				stopname = name.text.split("add")
			stop = stopname[0].replace('&amp;', '&')
			stoparray.append(stop)
			stops[stop] = stoplink.replace('simpleStopSelector', 'simplePrediction')
			stops[stop] = stoplink.replace('simpleToStopSelector', 'simplePrediction')
		self.stoparray = stoparray
		self.stops = stops

class Session:

	def __init__(self, route):
		self.current_route = route

	def open(self):
		
		for i in range(0, len(self.current_route.stoparray)):
			print str(i) + " => " + str(self.current_route.stoparray[i])  
		stopindex = raw_input("Which stop? ")
		while True:
			arrivals = self.current_route.get_estimate(self.current_route.stops[self.current_route.stoparray[int(stopindex)]])
			print "Buses arriving at " + self.current_route.stoparray[int(stopindex)] + " in: "
			for a in arrivals:
				print a
			print "Minutes"
			time.sleep(10)


if __name__ == '__main__':
	route_name = raw_input("Which route? ").upper()
	r = Route(route_name)
	for i in range(0, len(r.directionslist)):
		print str(i) + " => " + str(r.directionslist[i])
	direction = raw_input("Which direction? ")
	r.populate_stops(r.directionslist[int(direction)])
	s = Session(r)
	s.open()
